//
//  BugCharacter.m
//  SpaceViking
//
//  Created by anton on 9/8/13.
//
//

#import "Bug.h"

@interface BugCharacter (privateMethods)

-(void) stopWalkingEvent;
-(CGFloat) calcAngleBetweenPoint: (const CGPoint)p1 andPoint:(const CGPoint)p2;
@end

@implementation BugCharacter
@synthesize bugMovingAnim;
@synthesize ball;


- (void) dealloc {
    [bugMovingAnim release];
    
    [super dealloc];
}
-(void)initAnimations {
    
    [self setBugMovingAnim:[self loadPlistForAnimationWithName:@"walkingAnim" 
                                                  andClassName:@"Bug"]];
}

-(void) stopWalkingEvent
{
    isStopWalking = YES;
}

-(void)applyMoving:(float)deltaTime
{
    CGPoint oldPosition = [self position];
    CGPoint newPosition = ccp(oldPosition.x + 10.0f*deltaTime, oldPosition.y);
    [self setPosition:newPosition];
    
    [self checkAndClampSpritePosition];
    
   /* if (oldPosition.x > newPosition.x) {
        self.flipX = YES;                                        // 3
    } else if (oldPosition.x < newPosition.x){
        self.flipX = NO;
    }*/
}


-(void)updateStateWithDeltaTime:(ccTime)deltaTime andListOfGameObjects:(CCArray *)listOfGameObjects
{
    
    if (self.characterState == kStateDead)
        return;
    
    if( self.characterState == kStateAlmostDead)
    {  if( [self numberOfRunningActions] == 0 ) [self changeState:kStateDead];
      
        return;
    } 
    
    // check intersect with ball
    CGRect ballBox, rectIntersect;
    CGRect myBoundingBox = [self adjustedBoundingBox];
    
    ballBox = [ball adjustedBoundingBox];
    rectIntersect = CGRectIntersection(myBoundingBox, ballBox);
    
    // if ball intersected with bug greater then 50%
    if( rectIntersect.size.height >= myBoundingBox.size.height * 0.5f &&
       rectIntersect.size.width  >= myBoundingBox.size.width * 0.5f
       )
    {   // bug has crashed
        [self changeState:kStateAlmostDead];
        return;
    }
  
    
      
    if (self.characterState == kStateSpawning)
        [self changeState:kStateWalking];
    else
        if((self.characterState == kStateWalking || self.characterState == kStateBypass) && isStopWalking == YES)
        [self changeState:kStateWalking];
   
    if( fabsf(self.position.x - ball.position.x) < 100 &&
        fabsf(self.position.y - ball.position.y) < 100 && self.characterState != kStateBypass )
    { [self changeState:kStateBypass]; }
    
}

-(CGFloat) calcAngleBetweenPoint: (const CGPoint)p1 andPoint:(const CGPoint)p2
{
    CGFloat angle;
    float adjacent = p1.x - p2.x;
    float opposite = p1.y - p2.y;
    
    angle = atan2f(adjacent, opposite);
    angle = CC_RADIANS_TO_DEGREES(angle);
    
    return angle +=180;   

}

-(void)changeState:(CharacterStates)newState {
    //[self stopAllActions];
    CGPoint newPos;
    float angle;
    //float rand;
    ccTime timeDistance/*, timeRotate*/;
    
    id action = nil;
    [self setCharacterState:newState];
    
    switch (newState) {
        case kStateSpawning:           
            action = [CCRepeatForever actionWithAction:[CCAnimate actionWithAnimation:bugMovingAnim restoreOriginalFrame:NO]];        
            
            [action setTag:0];
            break;
        
        case kStateBypass:
        case kStateWalking:
            
            [self stopActionByTag:1];
                       
            newPos = ccp(screenSize.width*CCRANDOM_0_1(),screenSize.height*CCRANDOM_0_1());            
            timeDistance = ccpDistance(self.position,newPos)/130.0f;            
            angle = [self calcAngleBetweenPoint:self.position andPoint:newPos];
            
            //timeRotate = (angle - [self rotation])/300;
                        
            isStopWalking = NO;
            
            action = [CCSequence actions:
                        //[CCPlace actionWithPosition:ccp(screenSize.width/2,screenSize.height/2)],
                        [CCRotateTo	 actionWithDuration:0.3f angle:angle],
                        [CCMoveTo actionWithDuration:timeDistance position:newPos],                        
                        [CCCallFunc actionWithTarget:self selector:@selector(stopWalkingEvent)],
                      nil];
            
            [action setTag:1];
            
            
            
            break;
            
        case kStateAlmostDead:
            
            [self stopAllActions];
            
            [self setDisplayFrame:[[CCSpriteFrameCache
                                    sharedSpriteFrameCache]
                                    spriteFrameByName:@"crashed_bug.png" ]];
            
            [self.parent reorderChild:self z:5];
            
            action = [CCFadeOut actionWithDuration:3.0f];
            [action setTag:2];
          //
           // [self setVisible:NO];
           //[self removeFromParentAndCleanup:YES];
           break;
        case kStateDead:
            
            [self removeFromParentAndCleanup:YES];
            
            break;
            
        default:
            break;
    }
    if (action != nil) {
        [self runAction:action];
        
                
    }

}


-(id) init {
    if( (self=[super init]) ) {      
       
        
        self.gameObjectType = kBugType;
        isStopWalking = YES;
 
       
        [self initAnimations];
        [self changeState: kStateSpawning];
        
    }
    return self;
}
@end
