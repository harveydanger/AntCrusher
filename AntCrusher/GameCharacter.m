//  GameCharacter.m
//  SpaceViking

#import "GameCharacter.h"

@implementation GameCharacter
@synthesize characterHealth;
@synthesize characterState; 

-(void) dealloc { 
    [super dealloc];
}

-(int)getWeaponDamage {
    // Default to zero damage
    CCLOG(@"getWeaponDamage should be overriden");
    return 0;
}

-(void)checkAndClampSpritePosition { 
    CGPoint currentSpritePosition = [self position];
    float imageWidthHalved = [self texture].contentSize.width * 0.13f;
    float imageHeightHalved = [self texture].contentSize.height * 0.13f;
   
        if (currentSpritePosition.x < imageWidthHalved) {
            [self setPosition:ccp(imageWidthHalved, currentSpritePosition.y)];
            currentSpritePosition.x = imageWidthHalved;
        }
        else
        if (currentSpritePosition.x > (screenSize.width- imageWidthHalved))
        {
            [self setPosition:ccp(screenSize.width - imageWidthHalved, currentSpritePosition.y)];
            currentSpritePosition.x = screenSize.width - imageWidthHalved;
        }
    
    if (currentSpritePosition.y < imageHeightHalved) {
        [self setPosition:ccp(currentSpritePosition.x, imageHeightHalved)];
    } else if (currentSpritePosition.y > (screenSize.height- imageHeightHalved)) {
        [self setPosition:ccp(currentSpritePosition.x, (screenSize.height- imageHeightHalved))];
    }
    
    
  }
@end
