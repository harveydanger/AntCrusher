//
//  ATBall.h
//  Bugball
//
//  Created by anton on 9/15/13.
//
//

#import "GameCharacter.h"

@interface BallObject : GameCharacter
{
    CGPoint ballVelocity;
    CCSprite *baller;
    CGPoint obstacleCenter;
    CGSize obstacleSize;
}
-(void) accelerometer:(UIAccelerometer *)accelerometer didAccelerate:(UIAcceleration *)acceleration;
@end
