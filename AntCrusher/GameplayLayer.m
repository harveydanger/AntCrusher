//  GameplayLayer.m
//  SpaceViking

#import "GameplayLayer.h"
#import "GameManager.h"
#import "Ball.h"
#import "obstacle.h"

@implementation GameplayLayer

- (void) dealloc {

    [super dealloc];
}


#pragma mark –
#pragma mark Update Method
-(void) update:(ccTime)deltaTime {
    CCArray *listOfGameObjects =
    [sceneSpriteBatchNode children];
    
    GameObject *tempChar;
    
    // are there alive bags?
    for (tempChar in listOfGameObjects)
    {
        if( [tempChar tag] == kBugCharacterTagValue ) break;
            
    }
    // if there are no alive bugs
    if( [tempChar tag] != kBugCharacterTagValue )
        [[GameManager sharedGameManager] runSceneWithID:kMainMenuScene];
    
    for (tempChar in listOfGameObjects)
    {
       if( [tempChar tag] == kBugCharacterTagValue )
       {
           [tempChar updateStateWithDeltaTime:deltaTime andListOfGameObjects:listOfGameObjects];
       }
       else if ([tempChar tag] == kBallTagValue)
       {
           [tempChar updateStateWithDeltaTime:deltaTime andListOfGameObjects:listOfGameObjects];
       }
    }


}

#pragma mark -
-(void)createObjectOfType:(GameObjectType)objectType 
               withHealth:(int)initialHealth
               atLocation:(CGPoint)spawnLocation 
               withZValue:(int)ZValue {
    
    switch(objectType)
    {
            case kBugType:
            CCLOG(@"Creating the Bug");
            BugCharacter *bugCharacter = [[BugCharacter alloc] initWithSpriteFrameName:@"bug1.png"];
            [bugCharacter setCharacterHealth:initialHealth];
            [bugCharacter setPosition:spawnLocation];
            [bugCharacter setBall:ball];
            [sceneSpriteBatchNode addChild:bugCharacter
                                         z:ZValue
                                       tag:kBugCharacterTagValue];
            [bugCharacter release];
            break;
        case kObstacleType:
            CCLOG(@"Creating the Obstacle");
            ObstacleObject *obstacle = [[ObstacleObject alloc] initWithSpriteFrameName:@"dyrka.png"];
            [obstacle setPosition:spawnLocation];
            //[obstacle setAnchorPoint:spawnLocation];
            [sceneSpriteBatchNode addChild:obstacle
                                         z:ZValue
                                       tag:kObstacleTagValue];
            
            [obstacle release];
            break;
        case kBallType:
            CCLOG(@"Creating the Ball");
           ball = [[BallObject alloc] initWithSpriteFrameName:@"ball.png"];
            [ball setPosition:spawnLocation];
            [sceneSpriteBatchNode addChild:ball
                                         z:ZValue
                                       tag:kBallTagValue];
      
            break;
            
        default: break;
    }    
}

-(void)createPhaserWithDirection:(PhaserDirection)phaserDirection andPosition:(CGPoint)spawnPosition {
    CCLOG(@"Placeholder for chapter 5, see below");
    return;
}

-(id)init {
    self = [super init];
    if (self != nil) {
        CGSize screenSize = [CCDirector sharedDirector].winSize; 
        // enable touches
       // self.isTouchEnabled = YES;
        self.isAccelerometerEnabled = YES;
        
        srandom(time(NULL)); // Seeds the random number generator
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {         
            [[CCSpriteFrameCache sharedSpriteFrameCache] 
             addSpriteFramesWithFile:@"bugball_attlas_iphone5.plist"];          // 1
            sceneSpriteBatchNode = 
            [CCSpriteBatchNode batchNodeWithFile:@"bugball_attlas_iphone5.png"]; // 2
        } else {
            [[CCSpriteFrameCache sharedSpriteFrameCache] 
            // addSpriteFramesWithFile:@"scene1atlasiPhone.plist"];          // 1
             addSpriteFramesWithFile:@"bugball_attlas_iphone.plist"]; 
            sceneSpriteBatchNode = 
           //[CCSpriteBatchNode batchNodeWithFile:@"scene1atlasiPhone.png"];// 2
            [CCSpriteBatchNode batchNodeWithFile:@"bugball_attlas_iphone.png"];
        }
 
        
        [self addChild:sceneSpriteBatchNode z:0];                // 3
        
        [self createObjectOfType:kObstacleType
                      withHealth:100
                      atLocation:ccp(screenSize.width * 0.2f,
                                     screenSize.height * 0.2f)
                      withZValue:10];
        
        [self createObjectOfType:kObstacleType
                      withHealth:100
                      atLocation:ccp(screenSize.width * 0.7f,
                                     screenSize.height * 0.7f)
                      withZValue:10];
        
        [self createObjectOfType:kObstacleType
                      withHealth:100
                      atLocation:ccp(screenSize.width * 0.2f,
                                     screenSize.height * 0.7f)
                      withZValue:10];
        
        [self createObjectOfType:kObstacleType
                      withHealth:100
                      atLocation:ccp(screenSize.width * 0.5f,
                                     screenSize.height * 0.5f)
                      withZValue:10];
        
        
        [self createObjectOfType:kObstacleType
                      withHealth:100
                      atLocation:ccp(screenSize.width * 0.8f,
                                     screenSize.height * 0.8f)
                      withZValue:10];

        
        [self createObjectOfType:kBallType
                      withHealth:100
                      atLocation:ccp(screenSize.width * 0.6f,
                                     screenSize.height * 0.6f)
                      withZValue:30];
            [self createObjectOfType:kBugType
                      withHealth:100
                      atLocation:ccp(screenSize.width * 0.5f,
                                     screenSize.height * 0.5f)
                      withZValue:20];
        
        [self createObjectOfType:kBugType
                      withHealth:100
                      atLocation:ccp(screenSize.width * 0.4f,
                                     screenSize.height * 0.4f)
                      withZValue:20];
        
        
        [self createObjectOfType:kBugType
                      withHealth:100
                      atLocation:ccp(screenSize.width * 0.8f,
                                     screenSize.height * 0.8f)
                      withZValue:20];
        
        [self createObjectOfType:kBugType
                      withHealth:100
                      atLocation:ccp(screenSize.width * 0.8f,
                                     screenSize.height * 0.8f)
                      withZValue:20];
        [self createObjectOfType:kBugType
                      withHealth:100
                      atLocation:ccp(screenSize.width * 0.8f,
                                     screenSize.height * 0.8f)
                      withZValue:20];
        [self createObjectOfType:kBugType
                      withHealth:100
                      atLocation:ccp(screenSize.width * 0.8f,
                                     screenSize.height * 0.8f)
                      withZValue:20];
        [self createObjectOfType:kBugType
                      withHealth:100
                      atLocation:ccp(screenSize.width * 0.8f,
                                     screenSize.height * 0.8f)
                      withZValue:20];
        [self createObjectOfType:kBugType
                      withHealth:100
                      atLocation:ccp(screenSize.width * 0.8f,
                                     screenSize.height * 0.8f)
                      withZValue:20];
        
        
        [self scheduleUpdate];                                   // 8
    }
    return self; 
}

-(void) accelerometer:(UIAccelerometer *)accelerometer didAccelerate:(UIAcceleration *)acceleration
{
    [ball accelerometer:accelerometer didAccelerate:acceleration];
}



@end
