//
//  BugCharacter.h
//  SpaceViking
//
//  Created by anton on 9/8/13.
//
//

#import "GameCharacter.h"

@interface BugCharacter : GameCharacter
{
    CCAnimation *bugMovingAnim;
    BOOL isStopWalking;
    GameCharacter *ball;
}

@property (nonatomic, retain) CCAnimation *bugMovingAnim;
@property (assign) CCSprite *ball;

@end
