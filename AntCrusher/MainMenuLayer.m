//  MainMenuLayer.m
//  SpaceViking
//
#import "MainMenuLayer.h"
@interface MainMenuLayer() 
-(void)displayMainMenu;
-(void)displaySceneSelection;
@end

@implementation MainMenuLayer
-(void)buyBook {
    [[GameManager sharedGameManager]
     openSiteWithLinkType:kLinkTypeBookSite];
}

-(void)showOptions {
    CCLOG(@"Show the Options screen");
    [[GameManager sharedGameManager] runSceneWithID:kOptionsScene];
}


-(void)playScene:(CCMenuItem*)itemPassedIn {

    if ([itemPassedIn tag] == 1) {
        [[GameManager sharedGameManager] runSceneWithID:kGameLevel1];
    }
    /*   if ([itemPassedIn tag] == 1) {
        [[GameManager sharedGameManager] runSceneWithID:kIntroScene];
    } else if ([itemPassedIn tag] == 2) {
        [[GameManager sharedGameManager] runSceneWithID:kCutSceneForLevel2];
    } else if ([itemPassedIn tag] == 3) {
        [[GameManager sharedGameManager] runSceneWithID:kGameLevel3];
    } else if ([itemPassedIn tag] == 4) {
        [[GameManager sharedGameManager] runSceneWithID:kGameLevel4];
    } else if ([itemPassedIn tag] == 5) {
        [[GameManager sharedGameManager] runSceneWithID:kGameLevel5];
    } else {
        CCLOG(@"Unexpected item.  Tag was: %d", [itemPassedIn tag]);
    }*/
}



-(void)playChorus {
/*   int soundToPlay = random() % 2;
    if (soundToPlay == 0) {
        PLAYSOUNDEFFECT(CHORUS1);
    } else {
        PLAYSOUNDEFFECT(CHORUS2);
    }*/
}

-(void)displayMainMenu {
    CGSize screenSize = [CCDirector sharedDirector].winSize; 
    if (sceneSelectMenu != nil) {
        [sceneSelectMenu removeFromParentAndCleanup:YES];
    }
    // Main Menu
   CCMenuItemImage *playGameButton = [CCMenuItemImage
                                       itemFromNormalImage:[GameManager sharedGameManager].imageNames.mainmenu_button_play
                                       selectedImage:[GameManager sharedGameManager].imageNames.mainmenu_button_play_pushed
                                       disabledImage:nil 
                                       target:self 
                                       selector:@selector(playScene:)];
    
    
   
    [playGameButton setTag:1];
    
    mainMenu = [CCMenu 
                menuWithItems:playGameButton/*,buyBookButton,optionsButton*/,nil];
    [mainMenu alignItemsVerticallyWithPadding:screenSize.height * 0.059f];
    [mainMenu setPosition:
     ccp(screenSize.width * 2.0f,
         screenSize.height / 2.0f)];
    id moveAction = 
    [CCMoveTo actionWithDuration:1.2f 
                        position:ccp(screenSize.width * 0.5f,
                                     screenSize.height/2.0f)];
    id moveEffect = [CCEaseIn actionWithAction:moveAction rate:1.0f];
    id playChorus = [CCCallFunc actionWithTarget:
                     self selector:@selector(playChorus)];
    id sequenceAction = [CCSequence actions:moveEffect,playChorus,nil];
    [mainMenu runAction:sequenceAction];
    [self addChild:mainMenu z:0 tag:kMainMenuTagValue];
    
    // Volume batton, help button, levels button
    CCMenuItemImage *volumeButton = [CCMenuItemImage
                                      itemFromNormalImage:[GameManager sharedGameManager].imageNames.mainmenu_button_volume
                                      selectedImage:[GameManager sharedGameManager].imageNames.mainmenu_button_volume
                                     // disabledImage:nil
                                      //target:self
                                     // selector:@selector(buyBook)
                                      ];
    
    CCMenuItemImage *levelsButton = [CCMenuItemImage
                                     itemFromNormalImage:[GameManager sharedGameManager].imageNames.mainmenu_button_levels
                                     selectedImage:[GameManager sharedGameManager].imageNames.mainmenu_button_levels
                                     // disabledImage:nil
                                     //target:self
                                     // selector:@selector(buyBook)
                                     ];
    
    CCMenuItemImage *helpButton = [CCMenuItemImage
                                     itemFromNormalImage:[GameManager sharedGameManager].imageNames.mainmenu_button_help
                                   selectedImage:[GameManager sharedGameManager].imageNames.mainmenu_button_help

                                     // disabledImage:nil
                                     //target:self
                                     // selector:@selector(buyBook)
                                     ];

    
    CCMenu *mainMenu2 = [CCMenu
                menuWithItems:volumeButton,levelsButton,helpButton,nil];
    [mainMenu2 alignItemsHorizontallyWithPadding:screenSize.width * 0.04f];
    
    [mainMenu2 setPosition:
     ccp(screenSize.width * 0.5f,
         screenSize.height * 0.16f)];
    
    [self addChild:mainMenu2 z:0 tag:kMainMenu2TagValue];


    
}
-(void) accelerometer:(UIAccelerometer *)accelerometer didAccelerate:(UIAcceleration *)acceleration
{
    
    CGSize screenSize = [CCDirector sharedDirector].winSize;
    CGPoint ballVelocity;
    
    CGPoint curPos = ccp([background position].x,[background position].y);
    CGPoint newPos;
    
    float sensitivity = 6.0f;
    ballVelocity.y =  acceleration.y * sensitivity;

    newPos = ccp(curPos.x- ballVelocity.y,curPos.y + ballVelocity.x);
    
    if( newPos.x - screenSize.width/2 > 10.0f )
        newPos.x = screenSize.width/2 + 10.0f;
    else
        if( newPos.x - screenSize.width/2 < -10.0f )
            newPos.x = screenSize.width/2 - 10.0f;
    
    [background setPosition:newPos];
}


-(id)init {
    self = [super init];
    if (self != nil) {
        CGSize screenSize = [CCDirector sharedDirector].winSize; 
        
        background =
          [CCSprite spriteWithFile:[GameManager sharedGameManager].imageNames.mainmenu_background];
         [background setPosition:ccp(screenSize.width/2,
                                    screenSize.height/2)];
        [self addChild:background];
        [self displayMainMenu];
        
        self.isAccelerometerEnabled = YES;
        
        
        
        
    }
    return self;
}


@end
