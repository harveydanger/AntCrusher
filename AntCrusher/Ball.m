//
//  ATBall.m
//  Bugball
//
//  Created by anton on 9/15/13.
//
//

#import "Ball.h"

#import "GameScene.h"
//#import "CCActionCatmullRom.h"

@implementation BallObject

-(void)changeState:(CharacterStates)newState
{
    id action = nil;
    [self setCharacterState:newState];
    
    switch (newState) {
        case kStateWalking: break;
        case kStateDead:
                //[self removeFromParentAndCleanup:YES];
            [[CCDirector sharedDirector] replaceScene:[GameScene node]];
                               break;
        case kStateAlmostDead:
            
            [self stopAllActions];          
            
            action = [CCSpawn actions:
                        [CCScaleTo actionWithDuration:1.0f scale:0.0f],
                        [CCFadeOut actionWithDuration:1.0f],
                        [CCSequence actions:
                            [CCMoveTo actionWithDuration:0.2f position:obstacleCenter],
                            [CCMoveTo actionWithDuration:0.8f position:ccp(obstacleCenter.x+(obstacleSize.height*0.03f),obstacleCenter.y-(obstacleSize.height*0.16))], nil]
                        ,nil];
            
            break;
        default:
            break;
    }
    
    if( action != nil ) [self runAction:action];
    
}

- (void)updateStateWithDeltaTime:(ccTime)deltaTime andListOfGameObjects:(CCArray *)listOfGameObjects
{
   // [super updateStateWithDeltaTime:deltaTime andListOfGameObjects:listOfGameObjects];

    if (characterState == kStateDead)
    {    
        return; // No need to change state further once I am dead
    }
    
    if( self.characterState == kStateAlmostDead)
    {  if( [self numberOfRunningActions] == 0 ) [self changeState:kStateDead];
        
        return;
    }
 
    CGPoint pos = self.position;
  //  CCLOG(@"pos: %f",pos.x);
    pos.y += ballVelocity.x;
    pos.x -=ballVelocity.y;
    
    self.position = pos;
    [self checkAndClampSpritePosition];
    
    // let's check collision with obstacle
    CGRect characterBox, rectIntersect;
    CGRect myBoundingBox = [self adjustedBoundingBox];
    
    
    
    for (GameObject *character in listOfGameObjects)
    {
        if ([character tag] != kObstacleTagValue)
            continue;
        
        characterBox = [character adjustedBoundingBox];
        rectIntersect = CGRectIntersection(myBoundingBox, characterBox);
        
        // if ball intersected with obstacle greater then 50%
        if( rectIntersect.size.height >= myBoundingBox.size.height * 0.9f &&
           rectIntersect.size.width  >= myBoundingBox.size.width * 0.9f
           )
        {   obstacleCenter = character.position;
            obstacleSize = characterBox.size;
            [self changeState:kStateAlmostDead];
        }       
    }
    
}

- (void) goToCenter
{
   //
  [self setPosition:ccp([self screenSize].width/2,[self screenSize].height/2)];
}

-(id)init
{
    if(self = [super init])
    {
        self.gameObjectType = kBallType;
        [self changeState:kStateWalking];
    }
   // [self schedule:@selector(updateStateWithDeltaTime:andListOfGameObjects:)];
    return self;
}
-(void) accelerometer:(UIAccelerometer *)accelerometer didAccelerate:(UIAcceleration *)acceleration
{
    float deceleration = 0.8f;
    float sensitivity = 6.0f;
    float maxVelocity = 200;
    ballVelocity.x = ballVelocity.x * deceleration + acceleration.x * sensitivity;
    ballVelocity.y = ballVelocity.y * deceleration + acceleration.y * sensitivity;
    
    if (ballVelocity.x > maxVelocity)
    {
        ballVelocity.x = maxVelocity;
    }
    else if (ballVelocity.x < - maxVelocity)
    {
        ballVelocity.x = - maxVelocity;
    }
    if (ballVelocity.y > maxVelocity)
    {
        ballVelocity.y = maxVelocity;
    }
    else if (ballVelocity.y < - maxVelocity)
    {
        ballVelocity.y = - maxVelocity;
    }
    
//    CCLOG(@"Ballvelocity.y=%f and x=%f",ballVelocity.y,ballVelocity.y);
}

@end
